
# comments ----------------------------------------------------------------

#compare estimated recoveries in out files to the values in camp


# packages ----------------------------------------------------------------

require(odbc)
require(eratools)
require(ggplot2)

# paths -------------------------------------------------------------------

path <- "C:\\Users\\folkesm\\Documents\\Projects\\salmon\\chinook\\ctc\\awg\\exploitationRateAnalysis\\analyses\\analysisYear2021\\data\\Canada_2021_ERA_OUT"



# data import -------------------------------------------------------------

out.filenames <- list.files(path = path, pattern = ".*[0-9].*CYR\\.OUT$", full.names = TRUE, recursive = T)
#this function only works on .out files with digits in the filename
output <- readOUTfileBY(out.filenames, sortyear=TRUE)


#turn the list of files into one data frame
res <- lapply(output, function(x){
	res <- x$mortalities$recoveries$dat.long
	res <- res[res$age != "TOTAL",]
	res$broodyear <- x$broodyear
	res$age <- as.integer(gsub("AGE", "", res$age))
	res$returnyear <- x$broodyear+res$age
	res$stock <- x$stock
	res
	
})

res.df <- do.call(rbind, res)
head(res.df)

stocks <- c("BQR","CHI", "HAR","MSH","PPS","RBT","SHU")

res.sub <- res.df[res.df$age %in% c(2:3) & res.df$stock %in% stocks,]
res.sub$source <- "outfiles"

#give colnames same as found in camp
res.sub$fera_name <- res.sub$fisherygear
res.sub$Age <- res.sub$age
res.sub$EstimatedNumber <- res.sub$value
res.sub$RunYear <- res.sub$returnyear
res.sub$Stock <- res.sub$stock
res.sub$fera_name[res.sub$fera_name=="_ESCAPE"] <- "ESCAPEMENT"
res.sub$fera_name[res.sub$fera_name=="CA ESC_STRAY"] <- "XCA ESC STRAY"
res.sub$fera_name[res.sub$fera_name=="US ESC_STRAY"] <- "XUS ESC STRAY"



#import camp recoveries
camp_conn <- getCampConnection("camp.config")

sql <- paste0("SELECT rl.Stock, rc.*, ff.*, ff.Name AS ff_name, fc.Name AS fc_name, fc.Description as fc_desc, fera.FisheryERAID, fera.Name as fera_name ", 
							"FROM WireTagCode rl ",
							"INNER JOIN CWDBRecovery rc ",
							"ON rl.TagCode = rc.TagCode ",
							"INNER JOIN CAMPFisheryFine ff ",
							"ON rc.Fishery = ff.FisheryFineID ",
							"INNER JOIN CAMPFisheryCoarse fc ",
							"ON ff.FisheryCoarseID = fc.FisheryCoarseID ",
							"INNER JOIN CAMPFisheryERA fera ",
							"ON fc.FisheryERAID = fera.FisheryERAID ",
							"WHERE rl.Stock IN ",
							paste0("(", "'", paste(stocks, collapse = "','"), "'", ")")
)


data.recoveries <- dbGetQuery(camp_conn, sql)
str(data.recoveries)
dbDisconnect(camp_conn)

data.recoveries$source <- "camp"
data.recoveries.sum <- aggregate(EstimatedNumber~Stock+Age+fera_name+RunYear+source, data=data.recoveries, sum)

colnames.common <- intersect(colnames(res.sub), colnames(data.recoveries.sum))

res.bind <- rbind(res.sub[,colnames.common], data.recoveries.sum[,colnames.common])
res.bind <-  res.bind[grep("cohort", res.bind$fera_name, ignore.case = T, invert = T),]
res.bind$source.sym <- 20
res.bind$source.sym[res.bind$source=="camp"] <- 1

res.bind$Age.f <- as.factor(res.bind$Age)
unique(res.bind$fera_name)

res.bind$EstimatedNumber[res.bind$EstimatedNumber==0] <- NA



lapply(unique(res.bind$Stock), function(stock){
	
p <- ggplot(data=res.bind[res.bind$Age %in% 2:3 & res.bind$Stock==stock & res.bind$RunYear>=2017,], aes(RunYear, EstimatedNumber,size=source, col=Age.f, shape=source))+
	geom_point()+
	scale_shape_manual(values=c(19,1))+
	scale_size_manual(values=c(1,2))+
	scale_color_manual(values=c('#E69F00', '#56B4E9'))+
	facet_wrap( vars(fera_name), scales = "free_y")+
	labs(title=stock)
	filename <- paste0("compare_recoveries_camp_outfile_", stock, ".png")
ggsave(filename = filename, plot = p, width = 30, height = 10)	

})

