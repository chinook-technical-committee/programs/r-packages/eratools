
# comments ----------------------------------------------------------------

#author: michael folkes
#this allow for exporting of a .psl file from camp dbase data
#and the plots section compares data with a prior psl file


# packages ----------------------------------------------------------------

rm(list=ls())


require(odbc)
require(DBI)


#user.settings.config needs to have these elements, defining paths:
#filepath.camp.config: camp.config
#data.tmp: "./" #where ever you temporarily store data

settings <- yaml::read_yaml("scripts/user.settings.config")
if(! "data.tmp" %in% names(settings)) stop("\nuser.settings.config file needs a variable called 'data.tmp'")

# data:import era fishery pnv & CNR data from camp -----------------------------------------------

conn_string <- readChar(settings$filepath.camp.config, file.info(settings$filepath.camp.config)$size)
camp_conn <- dbConnect(odbc::odbc(), .connection_string = conn_string)

tbls.camp <- c("CAMPFisheryERA", "REAMPNVParams", "REAMPNVData", "REAMCNRData")
data.camp <- lapply(tbls.camp, function(x)DBI::dbReadTable(camp_conn, x))
names(data.camp) <- tbls.camp
dbDisconnect(camp_conn)



# plot against a psl file -------------------------------------------------

filepath <- list.files(settings$path.era, "psl$", ignore.case = T, full.names = T, recursive = T)
filepath
data.pslfile <- eratools::readPSL(filepath)

data.pslfile.pnv <- Map(cbind, fisheryera80name=names(data.pslfile$data.pnv), data.pslfile$data.pnv)
data.pslfile.pnv <- do.call(rbind, data.pslfile.pnv)
data.pslfile.pnv <- data.pslfile.pnv[,c("fisheryera80name", "year", "age1", "age2","age3", "age4", "sublegalIM", "legalIM", "dropoff" )]
data.pslfile.pnv$source <- "pslfile"

REAMPNVData <- merge(data.camp$REAMPNVData, ctctools::defs$fisheryera[,c("fisheryera80", "fisheryera80name")], by.x = "ERAFishery", by.y = "fisheryera80", sort=F)
REAMPNVData$source <- "camp"

REAMPNVData <- subset(REAMPNVData, select=-ERAFishery)
REAMPNVData <- REAMPNVData[,c("fisheryera80name", setdiff(names(REAMPNVData),"fisheryera80name"))]
names(data.pslfile.pnv) <- names(REAMPNVData)
range(REAMPNVData$ERAYear)

data.combined <- rbind(data.pslfile.pnv, REAMPNVData)


idvar <- c("fisheryera80name", "ERAYear", "source")
varying <- setdiff(names(data.combined), idvar)
data.long <- reshape(data.combined, direction = "long", varying = list(varying), timevar = "variable", times = varying, v.names = "value")

data.long$gearfisheryera80name.f <- factor(data.long$fisheryera80name, levels=unique(data.long$fisheryera80name))

require(ggplot2)
lapply(unique(data.long$variable), function(variable){
  p <- ggplot(data.long[data.long$variable %in% variable,], aes(ERAYear, value, col=source, shape=source))+
    geom_line()+geom_point()+
    scale_shape_manual(values=c(1, 16))+
    facet_wrap(vars(fisheryera80name.f))+
    labs(title = variable)

  filename <- paste("camp_vs_pslfile", variable, ".png", sep="_")
  filepath <- paste(settings$data.tmp, filename, sep="/" )
  ggsave(filepath, p, width = 20, height = 10, units = "in", dpi = 400)
})




# data:build list of lists for export -------------------------------------
data.psl <- list()

#metadata
natmort.cols <- grep("natmort", colnames(data.camp$REAMPNVParams), ignore.case = TRUE)
data.psl$metadata <- list(NaturalMortRates=round(unlist(data.camp$REAMPNVParams[,natmort.cols]),1), FirstYearofData=data.camp$REAMPNVParams[,1], LastYearofData=data.camp$REAMPNVParams[,2])

#pnv
data.camp$REAMPNVData$ERAFisheryID <- data.camp$REAMPNVData$ERAFishery

data.psl$data.pnv <- lapply(split(data.camp$REAMPNVData, data.camp$REAMPNVData$ERAFisheryID), function(x){
  data.frame(age1=x$PNVAge1, age2=x$PNVAge2, age3=x$PNVAge3, age4=x$PNVAge4,sublegalIM=x$SubLegalIM, legalIM=x$LegalIM, dropoff=x$DropOff, year=x$ERAYear)
})
names(data.psl$data.pnv) <- data.camp$CAMPFisheryERA$Name[data.camp$CAMPFisheryERA$FisheryERAID %in% names(data.psl$data.pnv)]


#cnr
#subset to era fisheries needed:
CAMPFisheryERA.tmp <- data.camp$CAMPFisheryERA[data.camp$CAMPFisheryERA$Name %in% names(data.psl$data.pnv),]

data.psl$CNRCalcFlg <- lapply(split(CAMPFisheryERA.tmp, CAMPFisheryERA.tmp$FisheryERAID), function(x) {
  CNRCalcFlg.int <- as.integer(x$PNVCNRFlag)
  if(CNRCalcFlg.int==0) {
    res <- list(CNRCalcFlg.int=CNRCalcFlg.int)
  } else if(CNRCalcFlg.int==1) {
    data.cnr <- list(selectivity=list(legal=round(x$PNVLegalSensitivity,2) , sublegal=x$PNVSubLegalSensitivity))
    res <- list(CNRCalcFlg.int=CNRCalcFlg.int, data.cnr=data.cnr)
  }
  return(res)
})


CNRData <- lapply(split(data.camp$REAMCNRData, data.camp$REAMCNRData$ERAFishery), function(x){
  res.list <- lapply(split(x,x$ERAYear), function(x.year){
    list.tmp <- as.list(x.year[3:8])[!is.na(x.year[3:8])]
    names(list.tmp) <- NULL
    list.tmp
  })
  return(res.list)
})



for(i in names(CNRData)){
  data.psl$CNRCalcFlg[[i]]$data.cnr$data <- CNRData[[i]]
}

names(data.psl$CNRCalcFlg) <- names(data.psl$data.pnv)






# write output ------------------------------------------------------------



filename <- "test2024.psl"
filepath <- paste(settings$data.tmp, filename, sep = "/")
eratools::writePSL(data.psl, filepath)
