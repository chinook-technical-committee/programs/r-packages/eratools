# eratools

An R package of tools that aid with analysis of ERA output files. (Not the VB code!)

To install in R:
    
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/eratools")
```
<!--
You need to enter your GitLab username and password if you get the following error: Error in 'git2r_remote_ls'. 
Run this code instead, adding in your username and password:
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/eratools", credentials=git2r::cred_user_pass("username","password")
```
-->

Load `eratools` into memory:
```
require(eratools)
```

The function `writeScript` will make it easier to start things. The help file has an example:
```
?eratools::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "eratools")
```

To save and open a specific demo script (this one named "demo01"):
```
writeScript("demo01")

```
