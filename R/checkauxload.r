


#' (Aux files) check if escapement auxiliary file was loaded in the database
#'
#' @param cas_path Path to CAS database
#' @param aux_path Path to auxiliary csv file that is being checked
#' @param AuxFileColNames Character vector. The names of columns in the aux files. Default is supplied.
#' @param stock_code What is the agency code of the auxillary file
#'
#' @return
#' @export
#' 
#' @examples
#' \dontrun{
#' 
#' }
checkAuxEscLoad <- function(cas_path, aux_path, AuxFileColNames = c("recovery_id", "recovery_date", "period_type_id", "period", "species", "sex", "length", "length_code", "tag_code", "tag_status", "estimatation_level", "location_code", "psc_fishery_id", "estimated", "sample_type", "run_year", "recorded_mark", "catch_sample_id", "detect_mthd", "reporting_agency", "finescale"), stock_code){
  aux <- read.csv(aux_path,
                  col.names = AuxFileColNames, 
                  stringsAsFactors = FALSE,
                  header = FALSE)
  
  cn <- connectAccessDb(cas_path)
  
  casdf <- getSqlScriptResult(cn, "getCasEscAuxRecoveries.sql", 
                              list(stock_code = stock_code,
                                   fishery_code = 4008L))
  
  if (nrow(casdf) != nrow(aux)) {
    print("The number of records in the auxiliary file does not match the record in the database\n")
    print("check the output list for details")
  }
  
  comparecasaux <- list(nrecaux = nrow(aux),
                        nreccas = nrow(casdf),
                        diffnrec = nrow(casdf) - nrow(aux),
                        diffsumest = sum(casdf$EstimatedNumber) - sum(aux$estimated),
                        recordonlyaux = aux[!(aux$recovery_id %in% casdf$RecoveryId),],
                        recordonlycas = casdf[!(casdf$RecoveryId %in% aux$recovery_id),])
  
  return(comparecasaux)
  
}#END checkAuxEscLoad






#' (Aux files) Checks that auxillary recovery IDs exist in the database
#'
#' @param cas_path Path to CAS database
#' @param aux_path path to auxiliary csv file that is being checked
#' @param AuxFileColNames Character vector. The names of columns in the aux files. Default is supplied.
#' @param agency_code What is the agency code of the auxillary file
#'
#' @return A data frame of missing recoveries
#' @export
#'
#' @importFrom odbc dbConnect dbGetQuery dbDisconnect
#' @importFrom dplyr %>% distinct anti_join
#' 
#' @examples
#' \dontrun{
#' 
#' }
checkAuxLoad <- function(cas_path, aux_path, AuxFileColNames = c("recovery_id", "recovery_date", "period_type_id", "period", "species", "sex", "length", "length_code", "tag_code", "tag_status", "estimatation_level", "location_code", "psc_fishery_id", "estimated", "sample_type", "run_year", "recorded_mark", "catch_sample_id", "detect_mthd", "reporting_agency", "finescale"), agency_code){
  
  aux <- 
    read.csv(aux_path,
             col.names = AuxFileColNames, 
             stringsAsFactors = FALSE,
             header = FALSE) 
  
  cn <- connectAccessDb(cas_path)
  
  casdf <- 
    getSqlScriptResult(cn, "getCasAuxRecoveries.sql", 
                       list(agency_code = agency_code)) %>%
    distinct(RecoveryId, RunYear, EstimatedNumber)
  
  missing_recs <-
    aux %>%
    anti_join(casdf, by = c(recovery_id = "RecoveryId", run_year = "RunYear"))
  
  if (nrow(missing_recs)) {
    print("The number of records in the auxiliary file do not exist in the database\n")
    print("check the output list for details")
  }  
  
  return(list(aux = aux,
              missing_recs = missing_recs))
  
}#END checkAuxLoad
