

#' Append a year to all fisheries
#'
#' @param psllist A list. Output of \code{\link{readPSL}}.
#' @param newyear An integer of length one.  The four digit year to to add to
#'   all records.
#'
#' @return A list
#' @export
#'
#' @examples
#' \dontrun{
#' 
#' data.psl <- readPSL(filename = "STD81FISH_2020.psl")
#' 
#' #append an extra year to all fisheries (PNV and CNR tables)
#' data.psl.new <- appendPSL(psllist = data.psl, newyear = 2020)
#' 
#' #write the modified results to a flat table for editing:
#' exportPSLtoCSV(psllist = data.psl.new, filename = "testpsl.csv")
#' 
#' #import same csv file:
#' data.psl.new <- importCSVtoPSL(filename = "testpsl.csv")
#' 
#' #now write out to a psl file
#' writePSL(filename = "testpsl.psl", psllist = data.psl.new)
#'
#' }
appendPSL <- function(psllist, newyear=NA){
  
  psllist$metadata$LastYearofData <- newyear
  
  psllist$data.pnv <- lapply(psllist$data.pnv, function(x){
    x.new <- x[nrow(x),]
    x.new$year <- newyear
    return(rbind(x, x.new))
  })
  
  
  psllist$CNRCalcFlg <- lapply(psllist$CNRCalcFlg, function(x){
    if(x$CNRCalcFlg.int==1){
     
     cnr.new <- x$data.cnr$data[length(x$data.cnr$data)]
     names(cnr.new) <- newyear
     
     str <- cnr.new[[1]][length(cnr.new[[1]])]
     str1 <- substr(str,1,nchar(str)-4)
     cnr.new[[1]][length(cnr.new[[1]])] <- paste0(str1, newyear)
     x$data.cnr$data <- c(x$data.cnr$data, cnr.new)
     
     #update the vector of strings
     x$data.cnr$data.cnr.str <- c(x$data.cnr$data.cnr.str, paste(unlist(cnr.new), collapse = ","))
    }
    return(x)
  })
  
  return(psllist)
  
  
}#END appendPSL



#' Export PSL list to flat file of all columns together
#'
#' @param psllist A list. Same as created by \code{\link{redPSL}}.
#' @param filename A character string. The name of the csv file to export to.
#'
#' @return Nothing returned. A csv file is written.
#' @export
#'
#' @examples
#' \dontrun{
#' 
#' data.psl <- readPSL(filename = "STD81FISH_2020.psl")
#' 
#' #append an extra year to all fisheries (PNV and CNR tables)
#' data.psl.new <- appendPSL(psllist = data.psl, newyear = 2020)
#' 
#' #write the modified results to a flat table for editing:
#' exportPSLtoCSV(psllist = data.psl.new, filename = "testpsl.csv")
#' 
#' #import same csv file:
#' data.psl.new <- importCSVtoPSL(filename = "testpsl.csv")
#' 
#' #now write out to a psl file
#' writePSL(filename = "testpsl.psl", psllist = data.psl.new)
#'
#' }
exportPSLtoCSV <- function(psllist, filename=NA){
  
  if(is.na(filename)) filename <- paste0("psl_", Sys.Date(), ".csv" )
      
  fishery.names <- names(psllist$data.pnv)
  data.pnv <- psllist$data.pnv
  CNRCalcFlg <- psllist$CNRCalcFlg
  
  #this area builds a list, then into a vector, columns to write to a flat table for easier editing
  data.combined <- lapply(fishery.names, function(fishery){
    
    cols.order <- c('year', setdiff(colnames(data.pnv[[fishery]]), "year"))
    pnv.df <- data.frame(fishery =fishery, data.pnv[[fishery]][,cols.order], stringsAsFactors = FALSE)
    pnv.df <- cbind(t(unlist(psllist$metadata)), pnv.df)
    pnv.df[2:nrow(pnv.df),1:6] <- NA
    
    pnv.list <- split(pnv.df, seq(nrow(pnv.df)))
    names(pnv.list) <- pnv.df$year
    
    
    CNRCalcFlg.int <- CNRCalcFlg[[fishery]]$CNRCalcFlg.int
    
    pnv.list <- lapply(pnv.list, function(x) c(x, CNRCalcFlg.int))  
    
    if(CNRCalcFlg.int==1){
      
      selectivity <- unlist(CNRCalcFlg[[fishery]]$data.cnr$selectivity)
      #browser()
      cnr.list <- lapply(names(CNRCalcFlg[[fishery]]$data.cnr$data), function(cnryear){unlist(CNRCalcFlg[[fishery]]$data.cnr$data[[cnryear]]) })
      cnr.list <- lapply(cnr.list, function(x) c(selectivity, x))
      pnv.list <- Map(c,pnv.list, cnr.list)
    }
    return(pnv.list)
  })
  
  names(data.combined) <- fishery.names
  
  data.str <- unlist(lapply(data.combined, function(fishery){ unlist(lapply(fishery, function(year){ paste(unlist(year), collapse = ",")}))}))

  header <- paste(c("NM1", "NM2", "NM3", "NM4", "firstyear", "finalyear", "fishery", "year", "age1", "age2", "age3", "age4", "sublegalIM", "legalIM", "dropoff", "cnrflag", "legalselectivity", "sublegalselectivity"), collapse = ",")
  
  #testing the addition of cnr field names, if problems them comment this off:
  header <- paste(header, paste0("cnrfield", 1:5, collapse = ","),  sep = ",")
  
  data.str <- c(header, data.str)
  
  write(data.str, file = filename, sep = "")
  
}#END exportPSLtoCSV




#' Exract CNR data frame from PSL object
#'
#' @param data.psl 
#'
#' @return
#' @export
#'
#' @examples
extractCNR <- function(data.psl, listname=c("fishery.name", "fishery.numb"), fishery.map=ctctools::defs$fisheryera){
  
  listname <- match.arg(listname)
  
  cnr.bol <- sapply(data.psl$CNRCalcFlg, "[[", "CNRCalcFlg.int")==1
  
  cnr.list <- lapply(data.psl$CNRCalcFlg[cnr.bol], function(CNRCalcFlg.list){
      
   list.tmp <- lapply(CNRCalcFlg.list$data.cnr$data, function(x) {
        
        names(x)[1] <- "cnr.method"
        
        if(x$cnr.method==0) {
          names(x)[2:4] <- c("dummy1", "dummy2", "comment")
          }
        else if(x$cnr.method==1) {
          names(x)[2:4] <- c("season.length.legal", "season.length.cnr", "comment")
          }
        else if(x$cnr.method==2) {
          names(x)[2:5] <- c("encounters.legal", "encounters.sublegal", "catch.landed", "comment")
        }
        else if(x$cnr.method==3) {
          names(x)[2:5] <- c("boatdays.cnr", "season.length.cnr", "reavalability", "comment")
        }
        c(cnr.fishery=1, x)
        
        })
   
   names(list.tmp) <- paste0("year", names(list.tmp))
   list.tmp
  })#END lapply(data.psl$CNRCalcFlg[[cnr.bol]]
  
  if(listname=="fishery.name"){
    name.tmp <- names(cnr.bol)[cnr.bol==TRUE]
    name.tmp <- gsub(" ", "-", name.tmp)
  }else{
    name.tmp <- fishery.map$fisheryera80[fishery.map$fisheryera80name %in% names(cnr.bol)[cnr.bol==TRUE]]
    #name.tmp <- defs$fishery.era.model$fishery.era[defs$fishery.era.model$fishery.era.name %in% names(cnr.bol)[cnr.bol==TRUE]]
  }
  
  names(cnr.list) <- paste0("fishery.era", name.tmp)
  
  cnr.list
  
}#END extractCNR


extractCNR.old <- function(data.psl){
  
  fisheries.era <- names(data.psl$CNRCalcFlg)
  
  cnr.list <- lapply(fisheries.era, function(fishery){
    # browser()
    if(data.psl$CNRCalcFlg[[fishery]]$CNRCalcFlg.int==1){
      res <- do.call(rbind, lapply(data.psl$CNRCalcFlg[[fishery]]$data.cnr$data, function(x) as.data.frame(x, col.names=paste0("v",1:5))))
      res$year <- as.integer(row.names(res))
      res$fisheryERA <- fishery
      res
    }
  })
  
  data.cnr <- do.call(rbind, cnr.list)
  row.names(data.cnr) <- NULL
  data.cnr
  
}#END extractCNR





#' Import a csv file of PNV/CNR data
#'
#' @param filename A character string length one.
#'
#' @return A list with same structure as produced by \code{\link{readPSL}}.
#' @export
#'
#' @examples
#' \dontrun{
#' 
#' 
#' data.psl <- readPSL(filename = "STD81FISH_2020.psl")
#' 
#' #append an extra year to all fisheries (PNV and CNR tables)
#' data.psl.new <- appendPSL(psllist = data.psl, newyear = 2020)
#' 
#' #write the modified results to a flat table for editing:
#' exportPSLtoCSV(psllist = data.psl.new, filename = "testpsl.csv")
#' 
#' #import same csv file:
#' data.psl.new <- importCSVtoPSL(filename = "testpsl.csv")
#' 
#' #now write out to a psl file
#' writePSL(filename = "testpsl.psl", psllist = data.psl.new)
#'
#' 
#' }
importCSVtoPSL <- function(filename){
  results <- list()
  #file can have variable number of columns, so need to check before import:
  ncol <- max(count.fields(filename, sep = ","))
  dat.tmp <- read.csv(filename, header = FALSE, skip = 1, stringsAsFactors = FALSE, fill = TRUE, col.names=paste0('V', seq_len(ncol)))
  #browser()
  results$metadata$NaturalMortRates <- as.numeric(dat.tmp[1,1:4])
  results$metadata$FirstYearofData <- as.integer(dat.tmp[1,5])
  results$metadata$LastYearofData <- as.integer(dat.tmp[1,6])
  
  fisheries.names <- unique(dat.tmp[,7])
  
  results$data.pnv <- lapply(fisheries.names, function(fishery){
    dat.tmp.fishery <- dat.tmp[dat.tmp[,7]==fishery,]
    data.pnv <- dat.tmp.fishery[,7:15]
    colnames(data.pnv) <- c("fishery", "year", "age1", "age2", "age3", "age4", "sublegalIM", "legalIM", "dropoff")
    return(data.pnv)
  })
  
  names(results$data.pnv) <- fisheries.names
  
  results$CNRCalcFlg <- lapply(fisheries.names, function(fishery){
    res.cnr <- list()
    
    dat.tmp.fishery <- dat.tmp[dat.tmp[,7]==fishery,]
    res.cnr$CNRCalcFlg.int <- CNRCalcFlg.int <- dat.tmp.fishery[1,16]
    res.cnr$CNRCalcFlg <- paste(CNRCalcFlg.int, "CNRCalcFlg: 0 if no CNR/1 if CNR (for this fishery only)", sep=",")
    
    if(CNRCalcFlg.int==1){
     
      res.cnr$data.cnr$selectivity$legal <- dat.tmp.fishery[1,17]
      res.cnr$data.cnr$selectivity$sublegal <- dat.tmp.fishery[1,18]
      data.cnr.str <- apply(dat.tmp.fishery[,19:ncol(dat.tmp.fishery)],1, function(x) {
        str <- paste((trimws(x)), collapse=",")
        str <- gsub(",$", "", str)
        return(str)
      })
      
      res.cnr$data.cnr$data.cnr.str <- data.cnr.str
      
      res.cnr$data.cnr$data <- apply(dat.tmp.fishery[,19:ncol(dat.tmp.fishery)],1, function(x) { 
          type.convert(as.list(trimws(x[x!=""])), as.is=TRUE) })
      
      names(res.cnr$data.cnr$data) <- seq(results$metadata$FirstYearofData, length.out = length(res.cnr$data.cnr$data))
    }#if(CNRCalcFlg.int==1)
    
    return(res.cnr)
    
  })
  
  names(results$CNRCalcFlg) <- fisheries.names
  
  return(results)
  
  
}#END importCSVtoPSL






#' Read PSL file into a list
#'
#' @param filename Character string of length one.  The name of the PSL file.
#'
#' @return A list
#' @export
#'
#' @examples
#' \dontrun{
#' 
#' filename <- list.files(pattern = "psl$", ignore.case = TRUE)
#' data.psl <- readPSL(filename = "STD81FISH_2020.psl")
#' 
#' #append an extra year to all fisheries (PNV and CNR tables)
#' data.psl.new <- appendPSL(psllist = data.psl, newyear = 2020)
#' 
#' #write the modified results to a flat table for editing:
#' exportPSLtoCSV(psllist = data.psl.new, filename = "testpsl.csv")
#' 
#' #import same csv file:
#' data.psl.new <- importCSVtoPSL(filename = "testpsl.csv")
#' 
#' #now write out to a psl file
#' writePSL(filename = "testpsl.psl", psllist = data.psl.new)
#' 
#' data.psl$metadata
#' data.psl$data.pnv$`AK W/S T`
#' data.psl$data.pnv$`TCAN TBR N`
#' 
#' #example of data where cnrflag is zero
#' data.psl$CNRCalcFlg$`AK W/S T`$CNRCalcFlg
#' data.psl$CNRCalcFlg$`AK W/S T`$CNRCalcFlg.int
#' 
#' #example of data where cnrflag is 1
#' data.psl$CNRCalcFlg$`AK JLO T`$CNRCalcFlg
#' data.psl$CNRCalcFlg$`AK JLO T`$data.cnr$selectivity
#' data.psl$CNRCalcFlg$`AK JLO T`$data.cnr$data.cnr.str[1:20]
#' data.psl$CNRCalcFlg$`AK JLO T`$data.cnr$data[1:20]
#' 
#' }
readPSL <- function(filename){
  results <- list()
  
  dat <- readLines(filename)
  
  metadata <- strsplit(dat[1:3], split = ",")
  names(metadata) <- lapply(metadata, function(x) gsub(" ", "", x[length(x)]))
  metadata <- lapply(metadata, function(x) type.convert(x[-length(x)]))
  metadata[2:3] <- lapply(metadata[2:3], function(x) x+1900)
  results$metadata <- metadata
  
  years.n <- metadata$LastYearofData-metadata$FirstYearofData+1
  dat <- dat[-(1:3)]
  comment.rows <- grep(pattern = "^'", dat)
  dat <- dat[-comment.rows]
  fishery.row <- grep("^[a-z|A-Z]", dat)
  fishery.names <- dat[fishery.row]
  
  data.pnv.rows <- (sapply(fishery.row, function(x) seq(x+1, length.out = years.n)))
  
  #_________________________________________________
  #data read error checking
  
  pnv.bad <- NULL
  cnr.bad <- NULL
  
  #this checks that pnv row count matches years expected. 
  #rows start with 1,0 or 0.#
  pnv.yearcount <- apply(data.pnv.rows, 2, function(x){
    sum(grepl("^1,0|^1\\.[0-9]+|^0\\.[0-9]+|^0,0", dat[x]))
  })
  
  if(any(pnv.yearcount != years.n)) {
   pnv.bad <- fishery.names[pnv.yearcount != years.n]
   pnv.bad <- paste(pnv.bad, collapse = "\n")
   pnv.bad <- paste("PNV data row count doesn't match range of years for:\n", pnv.bad,"\n", "Or specifically, one of the rows doesn't start with a '1' or '0.'.")
  }
  
  #checking that nrows of cnr matches year range
  cnr.yearcount <- apply(data.pnv.rows , 2, function(x) {
  
      #go one index beyond the last pnv row to get cnrflag:
      CNRCalcFlg.row.ind <- x[length(x)]+1
      CNRCalcFlg <- dat[CNRCalcFlg.row.ind]
      CNRCalcFlg.int <- as.integer(substr(CNRCalcFlg,start = 1,1))
      
      if(CNRCalcFlg.int==1){
        data.cnr.str <- dat[seq(CNRCalcFlg.row.ind+3, length.out = years.n)]
        
        #checking that nrows of cnr matches year range, by expecting any of 0:3 in first column:
        sum(grepl("^[0-3,]", data.cnr.str))
      }   
    })#END apply(data.pnv.rows , 2, function(x)  
  
  cnr.yearcount <- lapply(cnr.yearcount, function(x) {
      ifelse(is.null(x),NA,x)  })
  
  cnr.yearcount <- unlist(cnr.yearcount)
  
  if(any(cnr.yearcount[!is.na(cnr.yearcount)] != years.n)) {
    cnr.bad <- fishery.names[cnr.yearcount != years.n]
    cnr.bad <- cnr.bad[!is.na(cnr.bad)]
    cnr.bad <- paste(cnr.bad, collapse = "\n")
    cnr.bad <- paste("CNR data row count doesn't match range of years for:\n", cnr.bad,"\n", "Or specifically, one of the rows doesn't start with numbers ranging 0-3.")
  }
  
  #combine all the error results and stop the function if there are errors:
 msg <- paste(c(pnv.bad, cnr.bad), collapse = "\n\n\n")
  if(!is.null(pnv.bad)|!is.null(cnr.bad)) stop(msg)
  
  #end of error checking  
  #_______________________________________
 
 
 
 data.pnv <- apply(data.pnv.rows , 2, function(x) strsplit(dat[x], split = ","))
 data.pnv <- lapply(data.pnv, function(x){
   df <- as.data.frame(type.convert(do.call(rbind, x)))
   colnames(df) <- c("age1", "age2", "age3", "age4", "sublegalIM", "legalIM", "dropoff")
   df$year <- results$metadata$FirstYearofData:results$metadata$LastYearofData
   return(df)
 })
 
 names(data.pnv) <- fishery.names
 results$data.pnv <- data.pnv
 
 #grab all the cnr rows (by looking at final pnv row+1)
 CNRCalcFlg <- apply(data.pnv.rows , 2, function(x) {
   results.CNRCalcFlg <- list()
   
   #go one index beyond the last pnv row to get cnrflag:
   CNRCalcFlg.row.ind <- x[length(x)]+1
   results.CNRCalcFlg$CNRCalcFlg <- dat[CNRCalcFlg.row.ind]
   results.CNRCalcFlg$CNRCalcFlg.int <- as.integer(substr(results.CNRCalcFlg$CNRCalcFlg,start = 1,1))
   
   if(results.CNRCalcFlg$CNRCalcFlg.int==1){
     
     results.CNRCalcFlg$data.cnr$selectivity$legal <- as.numeric(strsplit(dat[CNRCalcFlg.row.ind+1],split = ",")[[1]][[1]])
     results.CNRCalcFlg$data.cnr$selectivity$sublegal <- as.numeric(strsplit(dat[CNRCalcFlg.row.ind+2],split = ",")[[1]][[1]])
     
     results.CNRCalcFlg$data.cnr$data.cnr.str <- data.cnr.str <- dat[seq(CNRCalcFlg.row.ind+3, length.out = years.n)]
     
     #checking that nrows of cnr matches year range, by expecting any of 0:3 in first column:
     
     if(sum(grepl("^[0-3,]", data.cnr.str)) !=years.n ){
       msg <- paste("CNR data row count doesn't match range of years for\n", dat[x[1]-1], "\nOr specifically, one of the rows doesn't start with numbers ranging 0-3.")
       stop(msg)
     }
     
     results.CNRCalcFlg$data.cnr$data <- lapply(data.cnr.str, function(x){
     cnr.list <- as.list(unlist(strsplit(x, split=",")))
     
     #adding NA to make all lists equal length
     # if(length(cnr.list)==4){
     #   #0 or 1
     #   cnr.list <- c(cnr.list[1:3], NA, cnr.list[4])
     # }
     
     type.convert(as.list(cnr.list), as.is = TRUE)
    })
    
    names(results.CNRCalcFlg$data.cnr$data) <- results$metadata$FirstYearofData:results$metadata$LastYearofData
     
    #return(results.CNRCalcFlg)
   }#END if(results.CNRCalcFlg$CNRCalcFlg==1)
   
   return(results.CNRCalcFlg)
   
   })#END apply(data.pnv.rows , 2, function(x)
  
 names(CNRCalcFlg) <- fishery.names
 
 results$CNRCalcFlg <- CNRCalcFlg

 return(results)
 
  
}#END readPSL



#' Write PSL list to PSL file
#'
#' @param psllist A list. Output of \code{\link{readPSL}}
#' @param filename Character string of length one. The name of file to write to.
#'   Default name is current date.
#'
#' @return NULL. A PSL text file is written.
#' @export
#'
#' @examples
#' \dontrun{
#'
#' data.psl <- readPSL(filename = "STD81FISH_2020.psl")
#'
#' #if no filename is specified it uses current date as file name prefix:
#' writePSL(data.psl)
#' 
#' 
#' data.psl <- readPSL(filename = "STD81FISH_2020.psl")
#' 
#' #append an extra year to all fisheries (PNV and CNR tables)
#' data.psl.new <- appendPSL(psllist = data.psl, newyear = 2020)
#' 
#' #write the modified results to a flat table for editing:
#' exportPSLtoCSV(psllist = data.psl.new, filename = "testpsl.csv")
#' 
#' #import same csv file:
#' data.psl.new <- importCSVtoPSL(filename = "testpsl.csv")
#' 
#' #now write out to a psl file
#' writePSL(filename = "testpsl.psl", psllist = data.psl.new)
#'
#'
#' }
writePSL <- function(psllist, filename=NA){
  #this hidden function minimizes repetition of the write call
  .writepsl <- function(x, append=TRUE,...) write(x, append = append, file = filename)
  
  if(is.na(filename)) filename <- paste0(Sys.Date(), ".psl" )

  .writepsl(paste(paste(psllist$metadata[[1]], collapse = ","), names(psllist$metadata[1]), sep=","),append=FALSE)
  
  .writepsl(paste(psllist$metadata[[2]]-1900, names(psllist$metadata[2]), sep=","))
  .writepsl(paste(psllist$metadata[[3]]-1900, names(psllist$metadata[3]), sep=","))
  
  fishery.names <- names(psllist$data.pnv)
  invisible(
  lapply(fishery.names, function(fishery){
    
    #data.pnv <- subset(psllist$data.pnv[[fishery]], select = -year)
    data.pnv <- psllist$data.pnv[[fishery]]
    data.pnv <- data.pnv[,c("age1", "age2", "age3", "age4", "sublegalIM", "legalIM", "dropoff")]
    data.pnv <- format(data.pnv, scientific=FALSE)
    data.pnv.str <- apply(data.pnv, 1, function(x) paste(x, collapse = ","))
    .writepsl(fishery)
    .writepsl("'REM Following is PropNonVuln by year and age (sublegalIM legalIM dropoff)")
    .writepsl(data.pnv.str)
    
    cnrflag.str <- paste(psllist$CNRCalcFlg[[fishery]]$CNRCalcFlg.int, "CNRCalcFlg: 0 if no CNR/1 if CNR (for this fishery only)", sep = "," )
    .writepsl(cnrflag.str)
    
    if(psllist$CNRCalcFlg[[fishery]]$CNRCalcFlg.int==1) {
     
      .writepsl(paste(psllist$CNRCalcFlg[[fishery]]$data.cnr$selectivity$legal, "Legal Selectivity (required if CNRCalcFlg = 1)", sep=","))
      .writepsl(paste(psllist$CNRCalcFlg[[fishery]]$data.cnr$selectivity$sublegal, "Sub-Legal Selectivity (required if CNRCalcFlg = 1)", sep=","))
      
      lapply(psllist$CNRCalcFlg[[fishery]]$data.cnr$data, function(x) {
        
        x <- unlist(x)
        x <- x[!is.na(x)] #remove na in cnr records to return to 4 vs 5 column structure
        .writepsl(paste(x, collapse = ","))
        })
    
    }

  })
  )
  
}#END writePSL


